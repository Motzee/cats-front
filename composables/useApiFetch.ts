import type { UseFetchOptions } from 'nuxt/app';
import { useRequestHeaders } from "nuxt/app";

export function useApiFetch<T>(path: string, options: UseFetchOptions<T> = {}) {
    const config = useRuntimeConfig();

    let headers: any = {}

    //protection contre la faille CSRF pour les forms
    const token = useCookie('XSRF-TOKEN');
    if (token.value) {
        headers['X-XSRF-TOKEN'] = token.value as string;
    }

    if (process.server) {
        console.log(config.public.appUrl)
        headers = {
            ...headers,
            ...useRequestHeaders(["cookie"]),
            referer: config.public.appUrl,
        }
    }

    return useFetch(config.public.apiUrl + path, {
        credentials: "include",
        watch: false,   //évite que la requête se refasse à chaque édition de champ
        ...options,
        headers: {
            ...headers,
            ...options?.headers
        }
    });
}