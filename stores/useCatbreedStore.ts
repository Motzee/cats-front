import { defineStore } from "pinia";
import { useApiFetch } from "~/composables/useApiFetch";

type CatbreedInfo = {
    slug: string;
    name: string;
    picture: string;
    health: string;
    description: string;
}

export const useCatbreedStore = defineStore('catbreeds', () => {
    /*
        async function fetchList() {
            const { data, error } = await useApiFetch("/api/catbreeds");
            return data.value
        }
    */
    async function create(info: CatbreedInfo) {
        await useApiFetch("/sanctum/csrf-cookie");

        const create = await useApiFetch("/api/catbreeds", {
            method: "POST",
            body: info,
        });

        return create;
    }

    async function update(info: CatbreedInfo) {
        await useApiFetch("/sanctum/csrf-cookie");

        const update = await useApiFetch("/api/catbreeds/" + info.slug, {
            method: "PUT",
            body: info,
        });

        return update;
    }

    async function softDelete(info: CatbreedInfo) {
        await useApiFetch("/sanctum/csrf-cookie");

        const softDelete = await useApiFetch("/api/catbreeds/" + info.slug, {
            method: "DELETE",
            body: info,
        });

        return softDelete;
    }

    return { /*fetchList*/ create, update, softDelete }
})