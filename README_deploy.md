# Requirements
- php 8.2 (for the API back)
- mysql 8.0 (for the API back)
- composer v2 (for the API back)
- node 18

# Deploy

WARNING ! This project need to be on the same domain as its API cats-back (https://gitlab.com/Motzee/cats-back)

1. Clone the project

```
npm install
```

2. Run the server of test and try `http://localhost:3000/`
```
npm run dev
```