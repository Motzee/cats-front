// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: { enabled: true },
    runtimeConfig: {
        //seul cet objet "public" sera exposé côté front
        public: {
            appUrl: process.env.NUXT_PUBLIC_APP_URL ?? "http://localhost:3000",
            apiUrl: process.env.NUXT_PUBLIC_API_URL ?? "http://localhost:8000"
        }
    },
    app: {
        head: {
            title: 'Catbreed'
        }
    },
    modules: [
        '@nuxtjs/tailwindcss',
        '@pinia/nuxt',
    ]
})
